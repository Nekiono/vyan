const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			requiredPermissions: ['MANAGE_ROLES'],
			runIn: ['text'],
			description: 'Adds or removes a role to everyone as a background task. May take up to 10 minutes depending on server size.',
			usage: '<set|remove|preview:default> [prefix|suffix] [text:str] [...]',
			quotedStringSupport: true,
			usageDelim: ' ',
			subcommands: true
		});

		this.donatorOnly = true;
	}

	async set(msg, [identifier, ...text]) {
		text = text.join(' ');
		if (!identifier || !text) return msg.responder.error('Missing parameter.');

		await msg.guild.settings.update(`nick${identifier.charAt(0).toUpperCase() + identifier.slice(1)}`, text);

		const { nickPrefix, nickSuffix } = msg.guild.settings;

		return msg.responder.success(`AutoNick updated, example: \`${nickPrefix || ''} ${msg.author.username} ${nickSuffix || ''}\`.`);
	}

	async remove(msg, [identifier]) {
		if (!identifier) return msg.responder.error('Missing parameter.');
		await msg.guild.settings.reset(`nick${identifier.charAt(0).toUpperCase() + identifier.slice(1)}`);

		return msg.reactor.success();
	}

	async preview(msg) {
		const { nickPrefix, nickSuffix } = msg.guild.settings;
		return msg.responder.info(`${nickPrefix || ''} ${msg.author.username} ${nickSuffix || ''}`);
	}

};
