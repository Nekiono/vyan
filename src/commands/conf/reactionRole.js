const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['rero', 'reactionRoles'],
			cooldown: 10,
			description: 'Add a reaction role. Like. Add reaction - get role.',
			runIn: ['text'],
			usage: '<add|remove>',
			subcommands: true
		});

		this.toDelete = new Set();
		this.message;
	}

	async add (msg) {
		const roles = await this.addMessage(msg);
		const m = await msg.channel.messages.fetch(this.message);
		if (!m) throw 'Invalid Message.';
		for (const [emote, role] of roles) {
			if (/<:(?:\w)+:(?:\d)+>/.test(emote)) {
				m.react(emote.match(/<:(?:\w)+:(\d+)>/)[1])
			} else {
				m.react(emote);
			}
		}
		const success = await this.addData(roles, msg);
		for (const _mes of this.toDelete) {
			const mes = msg.channel.messages.get(_mes);
			if (mes) mes.delete({ timeout: 100 });
		}
		msg.responder.success(`Added following reaction roles:\n${roles.map(i => i[0] + ' - ' + i[1].name).join('\n')}`);
	}

	async addMessage (msg) {
		const re = /^([0-9]{17,18})|(new)/i;
		const c = await this.ask('Which message do you want the reaction role(s) to be on?\nReply with the message ID below, or type "new" to create a collector on a new message.', re, msg, 60 * 1000);
		const res = await c.next
			.then(r => {
				this.toDelete.add(r.id);
				c.stop();
				return r.content.toLowerCase();
			});
		if (res === 'new') {
			const filter = m => (m.author.id === msg.author.id);
			const m = await msg.channel.send('Reply with the message below.');
			this.toDelete.add(m.id);
			const n = await msg.channel.createMessageCollector(filter, { time: 10 * 60 * 1000 });
			const { id } = await n.next;
			await n.stop();
			this.message = id;
		} else {
			this.message = res;
		}
		return this.addRoles(msg);
	}

	async addRoles (msg) {		
		const re = /((?:\u00a9|\u00ae|[\u2000-\u3300]|\ud83c[\ud000-\udfff]|\ud83d[\ud000-\udfff]|\ud83e[\ud000-\udfff]){1,2}|(?:<:(?:\w)+:(?:\d)+>))(?:\s*)(?:-)(?:\s*)?(.+)/g;
		const c = await this.ask('Please reply with the roles you want to add.\nFormat is `emote - role.` e.g.:\n```\n🌲 - Lorax\n❤ - LoveMe\n```', re, msg, 5 * 60 * 1000);
		const res = await c.next
			.then(n => {
				this.toDelete.add(n.id);
				c.stop();
				let r;
				const m = [];
				while (r = re.exec(n.content)) {
					m.push([r[1], r[2]]);
				}
				return m;
			})
			.then(n => {
				return n.map(i => [i[0], msg.guild.roles.find(r => r.name.toLowerCase() === i[1].toLowerCase())]);
			});
		if (res.some(i => !i[1])) throw 'Unknown role.';
		return res;
	}

	async addData (roles, msg) {
		roles = roles.map(r => [r[0], r[1].id]).map(i => {
			const e = i[0];
			const id = e.match(/<:(?:\w)+:(\d+)>/);
			if (id) return [id[1], i[1]];
			return [e, i[1]];
		});
		const existing = msg.guild.settings.reactionRoles.find(i => i.message === this.message && i.channel === msg.channel.id);
		let err;
		if (existing) {
			msg.guild.settings.update('reactionRoles', existing, { action: 'remove' });
			const merged = [...existing.reactions, ...roles].unique();
			err = await msg.guild.settings.update('reactionRoles', {
				message: this.message,
				channel: msg.channel.id,
				reactions: merged
			}, { action: 'add' }).then(r => r.error);
		} else {
			err = await  msg.guild.settings.update('reactionRoles', {
				message: this.message,
				channel: msg.channel.id,
				reactions: roles
			}, { action: 'add' }).then(r => r.error);
		}
		/*
		 * 	Format:
		 *	{
		 *  	message: <id>,
		 *		channel: <id>,
		 *		reactions: [
		 *			[<unicode_emote | emote_id>, <role_id>]
		 *		]
		 * 	}
		 */
		if (err) throw `Database Error: \`${err}\``
		return true;

	}

	remove () {
		throw 'To be added';
	}

	async ask(text, re, msg, time) {
		const r = new RegExp(re);
		const filter = m => ((m.author.id === msg.author.id) && r.test(m));
		const d = await msg.responder.info(text);
		this.toDelete.add(d.id);
		return msg.channel.createMessageCollector(filter, { time });
	}

};
