const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			runIn: ['text'],
			description: 'Sets up self-assignable roles.',
			usage: '<add|remove|list:default> [rolename:rolename]',
			usageDelim: ' ',
			subcommands: true
		});
	}

	async add(msg, [role]) {
		console.log(role);
		const { id } = role;
		if (msg.guild.settings.selfRoles.includes(id)) return msg.responder.error('That role is already set up.');
		await msg.guild.settings.update('selfRoles', id, msg.guild, { action: 'add' });
		return msg.responder.success(`**${role.name}** has been added to the list of self-assignable roles.`);
	}

	async remove(msg, [role]) {
		const { id } = role;
		if (!msg.guild.settings.selfRoles.includes(id)) return msg.responder.error('That role is not self-assignable.');
		await msg.guild.settings.update('selfRoles', id, msg.guild, { action: 'remove' });
		return msg.responder.success(`**${role.name}}** has been removed from the list of self-assignable roles.`);
	}

	async list(msg) {
		const roleNames = [];
		for (const selfRole of msg.guild.settings.selfRoles) {
			if (msg.guild.roles.has(selfRole)) {
				const role = await msg.guild.roles.get(selfRole);
				roleNames.push(role.name);
			}
		}
		return msg.responder.info(`Self-assignable roles on ${msg.guild.name}`, roleNames.sort().join('\n') || 'none');
	}

};
