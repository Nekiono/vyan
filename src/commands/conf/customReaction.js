/* eslint-disable id-length */
const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			requiredPermissions: ['SEND_MESSAGES', 'VIEW_CHANNEL', 'READ_MESSAGE_HISTORY'],
			runIn: ['text'],
			aliases: ['cr'],
			description: 'Manage CustomReactions',
			usage: '[add|list|remove] [index:int]',
			usageDelim: ' ',
			subcommands: true
		});
	}

	async run(msg) {
		this.list(msg);
	}

	async add(msg) {
		let trigger, response, regexYN, regex = false;
		try {
			msg.channel.send('What should the trigger of the reaction be?');
			trigger = await msg.channel.awaitMessages(message => message.author.id === msg.author.id, { max: 1, time: 30000, errors: ['time'] })
				.then(collected => collected.first().content);
			msg.channel.send('What should the response be?');
			response = await msg.channel.awaitMessages(message => message.author.id === msg.author.id, { max: 1, time: 60000, errors: ['time'] })
				.then(collected => collected.first().content);
			msg.channel.send('Should the reaction be regex?');
			regexYN = await msg.channel.awaitMessages(message => message.author.id === msg.author.id, { max: 1, time: 20000, errors: ['time'] })
				.then(collected => collected.first().content);
		} catch (_) {
			return msg.responder.error('Uh-oh, you took to long. Try again.');
		}

		if (regexYN.toLowerCase() === 'yes') regex = true;
		await msg.guild.settings.update('customReactions', { trigger: trigger, response: response, regex: regex }, { action: 'add' });
		return msg.channel.send({
			embed: {
				title: 'Custom reaction created.',
				fields: [
					{
						name: 'trigger',
						value: trigger
					},
					{
						name: 'response',
						value: response
					}
				],
				color: 5025616
			}
		});
	}

	async remove(msg, [index]) {
		if (!index) {
			msg.channel.send('Which reaction should be deleted?');
			index = await msg.channel.awaitMessages(message => message.author.id === msg.author.id, { max: 1, time: 10000, errors: ['time'] })
				.then(collected => collected.first().content);
			if (isNaN(index)) return msg.reactor.error();
		}
		if (index > msg.guild.settings.customReactions.length) return msg.reactor.error();
		const { trigger } = msg.guild.settings.customReactions[index - 1];
		await msg.guild.settings.update('customReactions', msg.guild.settings.customReactions[index - 1], { action: 'remove' });
		return msg.responder.success(`Deleted CustomReaction \`${trigger}\`.`);
	}

	async list(msg) {
		const message = [];
		for (let i = 0; i < msg.guild.settings.customReactions.length; i++) {
			message.push(`${i + 1}. ${msg.guild.settings.customReactions[i].trigger}`);
		}
		return msg.channel.send({
			embed: {
				fields: [
					{
						name: `CustomReactions on ${msg.guild.name}`,
						value: message.join('\n') || 'none'
					}
				]
			}
		});
	}

};
