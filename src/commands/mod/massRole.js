const { Command, util: { sleep } } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			requiredPermissions: ['MANAGE_ROLES'],
			runIn: ['text'],
			description: 'Adds or removes a role to everyone as a background task. May take up to 10 minutes depending on server size.',
			usage: '<role:rolename>',
			quotedStringSupport: true,
			permissionLevel: 10
		});

		this.running = new Set();
	}

	async run(msg, [role]) {
		if (msg.guild.me.roles.highest.rawPosition <= role.rawPosition) return msg.responder.error("I can't assign that role.");
		if (this.running.has(msg.guild.id)) return msg.responder.error('A massRole task is already running');
		await this.running.add(msg.guild.id);
		for (const member of msg.guild.members.filter(m => !m.roles.has(role.id)).values()) {
			if (!msg.flags.bot && member.user.bot) continue;
			member.roles.add(role.id);
			await sleep(1000);
		}
		await this.running.delete(msg.guild.id);
		return msg.reactor.success();
	}

};
