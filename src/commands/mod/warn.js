const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['w'],
			requiredPermissions: ['BAN_MEMBERS'],
			runIn: ['text'],
			description: 'Warns a mentioned user.',
			usage: '<member:user> <reason:string> [...]',
			usageDelim: ' '
		});
	}

	async run(msg, [user, ...reason]) {
		reason = reason.join(' ');
		user.send({ embed: {
			fields: [
				{
					name: `You have been warned on **${msg.guild.name}**.`,
					value: reason
				}
			],
			color: 16007990
		} });
		await msg.guild.settings.update('warns', {
			userID: user.id,
			usertag: user.tag,
			reason: reason
		}, { action: 'add' });
		await msg.guild.logger.warn({
			user,
			reason,
			moderator: msg.author
		});
		return msg.reactor.success();
	}

};
