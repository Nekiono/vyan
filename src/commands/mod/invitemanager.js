const { Command } = require('klasa');
const { Role } = require('discord.js');
const inviteRegex = /(?:https?:\/\/)?(?:www\.)?(?:discord\.gg|discordapp\.com\/invite)?(?:\/)?(?:\s)?(.+)/;

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['invm', 'invmgr'],
			runIn: ['text'],
			description: 'Manage invites for users.',
			subcommands: true,
			usage: '<give|take|infinite|revoke|list> [user:member|role:rolename] [amount:int|code:str]',
			usageDelim: ' '
		});
	}

	async give(msg, [member, amount = 1]) {
		if (!typeof member === 'object' || member instanceof Role) return;
		const { left } = member.settings.invites;
		await member.settings.update('invites.left', left + amount);
		return msg.responder.success(`Successfully updated invites for ${member}. New balance: **${left + amount}**.`);
	}

	async take(msg, [member, amount = 1]) {
		if (!typeof member === 'object' || member instanceof Role) return;
		const { left } = member.settings.invites;
		if (amount > left) amount = left;
		await member.settings.update('invites.left', left - amount);
		return msg.responder.success(`Successfully updated invites for ${member}. New balance: **${left - amount}**.`);
	}

	async infinite(msg, [memberOrRole]) {
		if (!msg.hasAtLeastPermissionLevel(14)) return msg.reactor.error();
		if (!memberOrRole) return msg.reactor.error();
		const type = memberOrRole instanceof Role ? 'Role' : 'User';
		await msg.guild.settings.update(`infinite${type}s`, memberOrRole.id, msg.guild).then(r => console.log(r.errors));
		return msg.responder.success(`Toggled creating infinite invites for ${memberOrRole}.`);
	}

	async revoke(msg, [member, invite]) {
		if (!member || member instanceof Role) return msg.responder.error('No member specified');
		if (!inviteRegex.test(invite)) return msg.responder.error('Invalid invite code');
		const code = inviteRegex.exec(invite)[1];
		if (!member.settings.invites.created.includes(code)) return msg.responder.error(`There is no active invite \`${code}\` by ${member}`);
		const currentInvs = await msg.member.guild.fetchInvites();
		if (currentInvs.has(code)) currentInvs.get(code).delete();
		await msg.member.settings.update('invites.created', code, { action: 'remove' });
		await msg.guild.settings.update('invites', { code, inviter: msg.member.id }, { action: 'remove' });
		return msg.responder.success(`Successfully revoked invite \`${code}\``);
	}

	async list(msg, [member]) {
		if (!member || member instanceof Role) return msg.responder.error('No member specified');
		const { created } = member.settings.invites;
		if (!created.length) return msg.responder.error("That member doesn't have any active invites.");
		return msg.author.send({ embed: {
			title: `${member}'s invites to **${msg.guild.name}**:`,
			description: `https://discord.gg/${created.join('\nhttps://discord.gg/')}`
		} });
	}

};
