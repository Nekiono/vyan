const { Command } = require('klasa');
const get = require('centra');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'Slap someone! Actually, please don\'t. It\'s not nice.',
			usage: '<somebody:user>',
			usageDelim: ' '
		});
	}

	async run(msg, [user]) {
		let self;
		if (user.id === msg.author.id) self = true;
		const { url } = await get('https://nekos.life/api/v2/img/slap').send().then(r => r.json());
		msg.channel.send({
			embed: {
				description: self ? `${user} slapped themselves. What.` : `${msg.author} slapped ${user}. Ouchie, that hurt 💔`,
				image: {
					url
				}
			}
		});
	}

};
