/* eslint-disable no-unused-vars */
const {
	Command,
	util: { isFunction }
} = require('klasa');
const { Collection, MessageEmbed } = require('discord.js');

const sliceCommandQuotes = /[《》]/g;
const sliceArgType = /:[a-zA-Z]+/g;
const sliceLimits = /{[0-9]+,[0-9]+}/g;
const sliceArgRepetition = /\[\.\.\.]/g;
const mods = ['admin', 'conf', 'fun', 'info', 'mod', 'nsfw', 'util', 'xp'];

const permissionNames = new Map()
	.set(0, 'None')
	.set(1, 'Network Supporter')
	.set(2, 'Network Moderator')
	.set(3, 'Network Administrator')
	.set(9, 'Bot Owner (God)')
	.set(10, 'Bot Owner (God)');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['commands', 'cmd', 'cmds', 'h'],
			guarded: true,
			runIn: ['text', 'dm'],
			description: language => language.get('COMMAND_HELP_DESCRIPTION'),
			usage: '(Command:command)'
		});

		this.createCustomResolver('command', (arg, possible, message) => {
			if (!arg || arg === '') return undefined;
			return this.client.arguments
				.get('command')
				.run(arg, possible, message);
		});
	}

	async run(msg, [command]) {
		const mdls = typeof command === 'string'
			? mods.includes(command)
				? [command]
				: undefined
			: mods;
		const commands = new Collection();
		for (const mdl of mdls) {
			const cmds = await this._fetchCommands(msg, mdl);
			commands.set(mdl, cmds);
		}
		if (command && typeof command !== 'string') {
			for (const [mdl, categories] of commands) {
				for (const [category, list] of categories) {
					if (list.includes(command)) {
						const embed = this.formatCommand(command, msg);
						return msg.send({ embed });
					}
				}
			}
			return msg.responder.error("That command couldn't be found.");
		}
		// if (!commands.size) return msg.responder.error('You can't use any commands from that module.');
		return this.sendHelp(commands, msg);
		// .then(() => { if (msg.channel.type !== 'dm') msg.sendMessage(msg.language.get('COMMAND_HELP_DM')); });
		// .catch(() => { if (msg.channel.type !== 'dm') msg.sendMessage(msg.language.get('COMMAND_HELP_NODM')); });
	}

	async _fetchCommands(message, mdl) {
		const run = this.client.inhibitors.run.bind(
			this.client.inhibitors,
			message
		);
		const commands = new Collection();
		await Promise.all(
			this.client.commands.map(command =>
				run(command, true)
					.then(() => {
						if (command.category === mdl) {
							const category = commands.get(command.subCategory);
							if (category) category.push(command);
							else commands.set(command.subCategory, [command]);
						}
					})
					.catch(() => {
						// noop
					})
			)
		);

		return commands;
	}

	formatCommand(command, message) {
		const description = isFunction(command.description)
			? command.description(message.language)
			: command.description;

		const sliceAliases = command.aliases.length
			? new RegExp(`\\u007C(${command.aliases.join('\\u007C')})`, 'gi')
			: '';
		const embed = new MessageEmbed()
			.setTitle(command.name)
			.setDescription(description);

		if (command.permissionLevel) embed.addField('» limited to', this._getPermissions(command));

		embed.addField(
			'» usage',
			`${this.client.options.prefix}${command.usage.nearlyFullUsage
				.replace(sliceArgType, '')
				.replace(sliceLimits, '')
				.replace(sliceArgRepetition, '')
				.replace(sliceAliases, '')
				.replace(sliceCommandQuotes, '')}`
		);
		embed.addField('» permission node',
			[command.category.toLowerCase(), command.name.toLowerCase()].join('.').replace('-', ''));
		embed.addField('» triggers', [command.name, ...command.aliases].join(', '));
		if (message.guild) embed.setColor(message.guild.me.displayColor);

		return embed;
	}

	_getPermissions(command) {
		return permissionNames.get(command.permissionLevel);
	}

	async sendHelp(commands, msg) {
		const embed = new MessageEmbed().setColor(
			msg.guild ? msg.guild.me.displayColor : 0x7fe582
		);
		for (const [mdl, categories] of commands) {
			let text = '';
			for (const [category, cmds] of categories) {
				text += category === 'General' ? '' : `__${category}__`;
				text += `\n${cmds.join(', ')}\n`;
			}
			if (text.length) embed.addField(`${mdl}`, text);
		}
		embed.setTitle('Command list')
			.setDescription([
				`To get more help on a command, use ${msg.guild.settings.prefix}help <command>.`,
				'Angle brackets <> indicate required arguments, square brackets [] indicate optional arguments.',
				`If you need more help, feel free to join our **[Community Server](https://discord.gg/${process.env.OPTIONS_SUPPORT_SERVER})**.`].join('\n'));
		return msg.send({ embed });
	}

};
