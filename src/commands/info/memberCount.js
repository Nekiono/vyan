const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['memcount', 'members'],
			requiredPermissions: ['SEND_MESSAGES', 'EMBED_LINKS'],
			runIn: ['text'],
			description: 'Displays the member count for the current guild.'
		});
	}

	async run(msg) {
		return msg.channel.send({
			embed: {
				author: {
					name: msg.guild.name,
					iconURL: msg.guild.iconURL({ format: 'png' })
				},
				description: `${msg.guild.memberCount} members:\n👥 ${msg.guild.members.filter(m => !m.user.bot).size} humans\n🤖 ${msg.guild.members.filter(m => m.user.bot).size} bots`
			}
		});
	}

};
