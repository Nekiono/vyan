const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			guarded: true,
			cooldown: 4,
			description: 'Says hi.'
		});
	}

	async run(msg) {
		msg.addReaction('hi');
	}

};
