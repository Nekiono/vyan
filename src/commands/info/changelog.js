const { Command } = require('klasa');
const changelogChannel = '469241522089492500';

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			runIn: ['text'],
			guarded: true,
			description: 'Get latest update for Vyan'
		});
	}

	async run(msg) {
		const channel = this.client.channels.get(changelogChannel);
		const changelog = await channel.messages.fetch({
			limit: 1
		}).then((msgs) => msgs.first().content.split('\n'));
		return msg.responder.info(changelog[0], changelog.slice(1).join('\n'));
	}

};
