const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['1337'],
			description: 'Make your input more 1337.',
			usage: '[text:str]'
		});
	}

	async run(msg, [text]) {
		return msg.responder.info(
			text
				.replace(/at/ig, '@')
				.replace(/a/ig, '4')
				.replace(/b/ig, '8')
				.replace(/e/ig, '3')
				.replace(/g/ig, '9')
				.replace(/i/ig, '1')
				.replace(/o/ig, '0')
				.replace(/s/ig, '5')
				.replace(/t/ig, '7')
		);
	}


};
