const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			guarded: true,
			cooldown: 2,
			description: 'Finds a user tag by ID.',
			usage: '<userID:str{17,18}>'
		});
	}

	async run(msg, [id]) {
		const user = await this.client.users.fetch(id);
		if (!user) return msg.reactor.error();
		return msg.responder.success(user.tag);
	}

};
