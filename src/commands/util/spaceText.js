const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 's p a c e   w o  r    d        s',
			usage: '[spaces:int{1,10}] <text:str> [...]',
			usageDelim: ' ',
			aliases: ['st'],
			cooldown: 2
		});
	}

	async run(msg, [power = 2, ...text]) {
		text = text.join(' ');
		let newText = '';
		if (msg.flags.power || msg.flags.p) {
			let i = 0;
			for (const char of text) {
				newText += char;
				for (let j = 0; j < power ** i; j++) {
					newText += ' ';
					if (newText.length > 2048) return msg.responder.error("That didn't work due to the 2000 character limit.");
				}
				i++;
				if (newText.length > 2048) return msg.responder.error("That didn't work due to the 2000 character limit.");
			}
		} else if (msg.flags.increment || msg.flags.i) {
			let i = 1;
			for (const char of text) {
				newText += char;
				for (let j = 0; j < power * i; j++) {
					newText += ' ';
					if (newText.length > 2048) return msg.responder.error("That didn't work due to the 2000 character limit.");
				}
				i++;
				if (newText.length > 2048) return msg.responder.error("That didn't work due to the 2000 character limit.");
			}
		} else {
			for (const char of text) {
				newText += char;
				for (let i = 0; i < power; i++) {
					newText += ' ';
					if (newText.length > 2048) return msg.responder.error("That didn't work due to the 2000 character limit.");
				}
				if (newText.length > 2048) return msg.responder.error("That didn't work due to the 2000 character limit.");
			}
		}
		if (msg.flags.delete) msg.delete();
		return msg.channel.send(newText);
	}

};
