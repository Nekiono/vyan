const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			cooldown: 5,
			description: 'Query for users in a role.',
			runIn: ['text'],
			usage: '<rolename:str>'
		});
	}

	async run(msg, [role]) {
		role = role.toLowerCase();
		if (!msg.guild.roles.some(r => r.name.toLowerCase() === role)) { return msg.channel.send(`A role by the name of '${role}' could not be found.`); }
		const members = await msg.guild.roles
			.find(r => r.name.toLowerCase() === role).members
			.map(member => msg.flags.name ? member.displayName : member.toString()).join('\n');
		return msg.channel.send({ embed: {
			title: `There are ${members.split('\n').length} members with the role ${role}.`,
			description: members
		} });
	}

};
