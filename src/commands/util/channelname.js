const { Command } = require('klasa');

const letters = {
	A: '𝖠',
	B: '𝖡',
	C: '𝖢',
	D: '𝖣',
	E: '𝖤',
	F: '𝖥',
	G: '𝖦',
	H: '𝖧',
	I: '𝖨',
	J: '𝖩',
	K: '𝖪',
	L: '𝖫',
	M: '𝖬',
	N: '𝖭',
	O: '𝖮',
	P: '𝖯',
	Q: '𝖰',
	R: '𝖱',
	S: '𝖲',
	T: '𝖳',
	U: '𝖴',
	V: '𝖵',
	W: '𝖶',
	X: '𝖷',
	Y: '𝖸',
	Z: '𝖹'
}

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'Set the channel name. Supports spaces and uppercase!!',
			usage: '[name:str]'
		});
	}

	async run(msg, [name]) {
		if (!msg.channel.permissionsFor(msg.member).has('MANAGE_CHANNELS')) return msg.responder.error('Insufficient Permissions.');
		await msg.channel.setName(name.replace(/\s/g, '\u2009\u2009').replace(/([A-Z])/g, match => letters[match]));
		return msg.reactor.success();
	}

};
