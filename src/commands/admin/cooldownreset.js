const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			permissionLevel: 10,
			guarded: true,
			aliases: ['cdr', 'resetcooldown', 'rcd'],
			description: 'Resets the cooldown of a user',
			usage: '<command:command> [user:user]',
			usageDelim: ' '
		});
	}

	async run(msg, [command, user]) {
		if (!user) user = msg.author;
		if (command.cooldowns.has(user.id)) command.cooldowns.acquire(user.id).reset();
		return msg.reactor.success();
	}

};
