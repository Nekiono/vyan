const { Extendable } = require('klasa');
const { Message } = require('discord.js');

module.exports = class extends Extendable {

	constructor(...args) {
		super(...args, { appliesTo: [Message] });
	}

	async ask(content) {
		const message = await this.send(content);
		if (this.channel.permissionsFor(this.guild.me).has('ADD_REACTIONS')) return awaitReaction(this, message);
		return awaitMessage(this);
	}

};

const awaitReaction = async (msg, message) => {
	await message.react('🇾');
	await message.react('🇳');
	const data = await message.awaitReactions(reaction => reaction.users.has(msg.author.id), { time: 20000, max: 1 });
	if (data.firstKey() === '🇾') return true;
	return false;
};

const awaitMessage = async (msg) => {
	const messages = await msg.channel.awaitMessages(mes => mes.author === msg.author, { time: 20000, max: 1 });
	if (messages.size === 0) return false;
	const message = await messages.first();
	if (message.content.toLowerCase() === 'yes') return true;
	return false;
};
