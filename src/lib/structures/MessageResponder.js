const { MessageEmbed } = require('discord.js');
const { colors, emojis } = require('../util/constants');

module.exports = class MessageResponder {

	constructor(message) {
		this.message = message;
	}

	success(content) {
		const embed = new MessageEmbed()
			.setColor(colors.success)
			.setDescription(`${emojis.success} ${content}`);
		return this.message.sendEmbed(embed);
	}

	error(content) {
		const embed = new MessageEmbed()
			.setColor(colors.error)
			.setDescription(`${emojis.error} ${content}`);
		return this.message.channel.sendEmbed(embed);
	}

	info(title, content, footer) {
		if (!content) {
			content = title;
			title = null;
		}
		const embed = new MessageEmbed()
			.setColor(colors.info)
			.setDescription(content);
		if (title) embed.setTitle(title);
		if (footer) embed.setFooter(footer);
		return this.message.channel.sendEmbed(embed);
	}

	embed(title, content, footer, type) {
		const color = colors[type];
		const embed = new MessageEmbed()
			.setTitle(title)
			.setDescription(content)
			.setFooter(footer)
			.setColor(color);
		return this.message.sendEmbed(embed);
	}

	image(url) {
		return this.message.channel.sendEmbed({ image: { url } });
	}

	emoji(emoji, content) {
		return this.message.channel.sendEmbed({ content: `${emojis[emoji]} ${content}` });
	}

	async awaitNumberedResponse(content, [start, end]) {
		await this.emoji('thinking', content);
		const responses = await this.message.channel.awaitMessages(msg => msg.author.id === this.message.author.id
			&& parseInt(msg.content)
			&& parseInt(msg.content) >= start
			&& parseInt(msg.content) <= end, { max: 1, time: 30 * 1000 });
		if (!responses.size) return this.error('No option received. Aborted prompt.');
		const response = responses.first();
		return parseInt(response.content);
	}

};

