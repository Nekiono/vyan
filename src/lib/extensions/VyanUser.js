const { Structures } = require('discord.js');
const MessageResponder = require('../structures/MessageResponder');

module.exports = Structures.extend('User', User => {
	class VyanUser extends User {

		constructor(...args) {
			super(...args);
			this.responder = new MessageResponder(this);
		}

	}

	return VyanUser;
});
