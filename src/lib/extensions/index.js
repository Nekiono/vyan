module.exports = {

	// lib/extensions
	VyanChannel: require('./VyanChannel'),
	VyanCommand: require('./VyanCommand'),
	VyanGuild: require('./VyanGuild'),
	VyanMember: require('./VyanMember'),
	VyanMessage: require('./VyanMessage'),
	VyanRole: require('./VyanRole'),
	VyanUser: require('./VyanUser')

};
