const { Structures } = require('discord.js');
const MessageResponder = require('../structures/MessageResponder');
const MessageReactor = require('../structures/MessageReactor');

module.exports = Structures.extend('Message', Message => {
	class VyanMessage extends Message {

		constructor(...args) {
			super(...args);
			this.responder = new MessageResponder(this);
			this.reactor = new MessageReactor(this);
		}

	}

	return VyanMessage;
});
