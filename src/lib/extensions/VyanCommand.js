const { Command } = require('klasa');

class VyanCommand extends Command {

	constructor(...args) {
		super(...args);
		this.donatorOnly = false;
	}

}

module.exports = VyanCommand;
