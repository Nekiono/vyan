const { Monitor } = require('klasa');
const bannedWords = ['kys', 'killyourself', 'killurself', 'killmyself', 'killme', 'kms'];

module.exports = class extends Monitor {

	constructor(...args) {
		super(...args, {
			name: 'kys',
			enabled: false,
			ignoreBots: true,
			ignoreSelf: true,
			ignoreOthers: false
		});
	}

	async run(msg) {
		if (!msg.guild || !msg.member) return;
		const cleanMsg = msg.content.toLowerCase().replace(/[^a-z]/gi, '');
		let triggered = false;
		bannedWords.every(word => {
			if (cleanMsg.includes(word)) {
				triggered = true;
				return false;
			} else {
				return true;
			}
		});
		if (triggered) {
			await msg.delete();
			await msg.author.settings.update('previous_profanity', 150);
			return msg.author.sendMessage(
				['While the use of the term `kys` or variations may seem innocent to you, please remember that this can truly, and very dangerously, affect some people.',
					'People suffering from depression can see this and essentially be triggered into having suicidal thoughts.',
					'That means **you** could be the cause of a loss of life, and we know you don\'t want that!',
					'We know you may mean it as just a meme, or maybe just as a variation on "go *bleep* yourself".',
					'Regardless of intent, the management on this server believes that it should not be used. Please refrain from doing this in the future.'
				].join('\n'))
				.catch(() => null);
		}
	}

};
