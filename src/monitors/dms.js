const { Monitor } = require('klasa');

module.exports = class extends Monitor {

	constructor(...args) {
		super(...args, {
			name: 'dms',
			enabled: true,
			ignoreSelf: true,
			ignoreBots: true,
			ignoreOthers: false
		});
	}

	async run(msg) {
		if (msg.channel.type !== 'dm') return null;
		if (msg.author.id === this.client.owner.id) return null;
		if (msg.command) return null;
		return this.client.owner.send({
			embed: {
				author: {
					name: msg.author.tag,
					iconURL: msg.author.avatarURL({ format: 'png' })
				},
				description: msg.content,
				timestamp: msg.createdAt,
				footer: {
					text: `author ID: ${msg.author.id}`
				}
			}
		});
	}

};
