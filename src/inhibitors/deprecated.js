const { Inhibitor } = require('klasa');

module.exports = class extends Inhibitor {

	constructor(...args) {
		super(...args, {
			enabled: true
		});
	}

	async run(message, command) {
		if (command.deprecated) throw 'This command has been deprecated.';
		return;
	}

};
