const { Event, util } = require('klasa');
const req = require('centra');

module.exports = class extends Event {

	async run(member) {
		await util.sleep(6000);
		if (member.presence.status === "offline") {
			// Ban the shit outta them
			const { user } = member;
			req('https://api.ksoft.si', 'POST')
				.path('/bans/add')
				.header('Authorization', `Bearer ${process.env.API_KSOFT}`)
				.body({
					user: user.id,
					mod: '142242854167642122',
					user_name: user.username,
					user_discriminator: user.discriminator,
					reason: "[Automated] Second massive wave of fake accounts doing DM Advertisements, circumstantial evidence since we can't manually check multiple hundreds of users: Joined in two weeks, similar username, some already reported and banned for DM Ads",
					proof: 'https://i.imgur.com/hoHok2C.png',
					appeal_possible: false
				}, 'form')
				.send();
			this.client.settings.update('syncBannedUsers', {
				user: user.id,
				requester: '102102717165506560',
				reason: "[Automated] Second massive wave of fake accounts doing DM Advertisements, circumstantial evidence since we can't manually check multiple hundreds of users: Joined in two weeks, similar username, some already reported and banned for DM Ads",
				proof: 'https://i.imgur.com/hoHok2C.png',
				moderator: '102102717165506560'
			}, { action: 'add' });
		}
	}

};
