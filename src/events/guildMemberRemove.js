const { Event } = require('klasa');
const { MessageEmbed } = require('discord.js');

module.exports = class extends Event {

	async run(member) {
		const { settings: { log: { members: logChannel } } } = member.guild;
		if (!logChannel) return;
		const channel = member.guild.channels.get(logChannel);
		const embed = new MessageEmbed()
			.setAuthor(`${member.user.bot ? 'Bot' : 'User'} left: ${member.user.tag}`, member.user.displayAvatarURL({ format: 'png', size: 256 }))
			.setFooter(`member #${member.guild.memberCount}`)
			.setColor(0xF44336);
		return channel.send({ embed });
	}

};
