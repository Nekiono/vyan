const { Event } = require('klasa');
const { MessageEmbed } = require('discord.js');
const { success, error } = require('../lib/util/constants').reactions;
const cID = '543846152072724500';
const gbr = '543880069882314772';
const vieID = '367759499613962240';

module.exports = class extends Event {

	async run(data) {
		const { client } = this;
		const user = await this.client.users.fetch(data.user);
		const vie = this.client.guilds.get(vieID);
		const aFilter = (r, u) => r.emoji.id === success && vie.members.get(u.id).roles.has(gbr);
		const dFilter = (r, u) => r.emoji.id === error && vie.members.get(u.id).roles.has(gbr);
		const embed = new MessageEmbed()
			.setTitle(`${user.tag} (${user.id})`)
			.setDescription(data.reason)
			.setImage(data.proof)
			.setFooter(`id: ${data.id}`);
		const msg = await this.client.channels.get(cID).send(embed);
		await msg.react(success);
		await msg.react(error);
		const cAccept = await msg.createReactionCollector(aFilter);
		const cDeny = await msg.createReactionCollector(dFilter);
		cAccept.on('collect', (r, u) => {
			cAccept.stop();
			cDeny.stop();
			return client.emit('reportAccepted', {
				data: {
					...data,
					moderator: u.id
				},
				message: {
					channel: msg.channel.id,
					id: msg.id
				}
			});
		});
		cDeny.on('collect', (r, u) => {
			cAccept.stop();
			cDeny.stop();
			return client.emit('reportDenied', {
				data: {
					...data,
					moderator: u.id
				},
				message: {
					channel: msg.channel.id,
					id: msg.id
				}
			});
		});
	}

};
